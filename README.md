# Ogre/Gorilla/OculusRift Example

Ogre + Gorilla + OculusRift => Demo

## Feature

* Sample for [Ogre 1.9](http://www.ogre3d.org/download)
* Barrel distortion for OculusRift (based on [OgreOculus][ogreoculus])
* OculusRift Head Tracker Plug and Play at Runtime (Based on OculusSDK 0.2.5)
* 3D GUI Sample from [gorilla][gorilla]
* Supports [CMake](http://www.cmake.org/cmake/resources/software.html)

## Installation / Build and run
1. Install [Ogre](http://www.ogre3d.org/download). You need to set the environment variable. **%OGRE_HOME%** to wherever you installed Ogre. Something like `D:/ogre19`
2. install [CMake](http://www.cmake.org/cmake/resources/software.html)
3. use git to clone this repo and don't forget to also pull in the referenced `cmake-lib` and `libovr` repos in the `../ext` directory.
3. open a terminal and do this: `mkdir build`
4. then `cd build`
5. then `cmake ..`
6. Then open `../build/demo.sln` in visual studio
7. Select all of the solutions in the Solution Explorer, right click, go to properties
8. Then set "Configuration" to "All Configurations"
9. Go to "VC++ Directories" and add `%OGRE_HOME%\boost;` to "Include Directories" and `%OGRE_HOME%\boost\lib` to "Library Directories"
10. Copy all the core Ogre .dll's from `%OGRE_HOME%\bin\Debug` to `..\bin\Debug` in the project dir. You don't need any of the .dll's named "Sample".
11. right click the **demo** solution and hit build.
12. build the **INSTALL** solution
13. in the solution browser go to Demo >> Properties >> Debugging >> Working Directory and set it to `$(ProjectDir)/dist/bin`

Finally, you can run the demo-- just hit F5 in Visual Studio.

## Controls
* wasd or left analog stick : Movement
* mouse move or right analog stick : Look
* OculusRift HeadTracker support
* mouse left click or joysitck A button : push button on 3D GUI
* esc : exits the demo if the console is not open
* F5 : Normal View
* F6 : Stereo View without barrel distortion
* F7 : OculusRift View. Stereo View with barrel distortion
* ~ : toggle console. this is from the gorilla sample.

[ogreoculus]: https://bitbucket.org/rajetic/ogreoculus
[gorilla]: https://github.com/betajaen/gorilla
