

#include "entrypoint.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	#define WIN32_LEAN_AND_MEAN
	#include "windows.h"
#endif

#if defined(_DEBUG) && OGRE_PLATFORM == OGRE_PLATFORM_WIN32 && !defined(__GNUWIN32__) && !defined(_WIN32_WCE)
	#define USE_MEMORY_LEAK_DETECT 1
#else
	#define USE_MEMORY_LEAK_DETECT 0
#endif

//#if USE_MEMORY_LEAK_DETECT
//#include <crtdbg.h>
//#endif


//INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
int main(int argc, char *argv[])
{
	int exit_code = entrypoint();
	return exit_code;
}