#include "stdafx.h"
#include "vrl_voxel_demo.hpp"

namespace VRL {
	Ogre::ManualObject * createCubeMesh (Ogre::String name, Ogre::String matName) {
		Ogre::ManualObject * cube = new Ogre::ManualObject (name);
		cube->begin (matName);
			// set up verts, normals, and uv's
			cube->position(0.5,-0.5,0.5);cube->normal(0.408248,-0.816497,0.408248);cube->textureCoord(1,0);
			cube->position(-0.5,-0.5,-0.5);cube->normal(-0.408248,-0.816497,-0.408248);cube->textureCoord(0,1);
			cube->position(0.5,-0.5,-0.5);cube->normal(0.666667,-0.333333,-0.666667);cube->textureCoord(1,1);
			cube->position(-0.5,-0.5,0.5);cube->normal(-0.666667,-0.333333,0.666667);cube->textureCoord(0,0);
			cube->position(0.5,0.5,0.5);cube->normal(0.666667,0.333333,0.666667);cube->textureCoord(1,0);
			cube->position(-0.5,-0.5,0.5);cube->normal(-0.666667,-0.333333,0.666667);cube->textureCoord(0,1);
			cube->position(0.5,-0.5,0.5);cube->normal(0.408248,-0.816497,0.408248);cube->textureCoord(1,1);
			cube->position(-0.5,0.5,0.5);cube->normal(-0.408248,0.816497,0.408248);cube->textureCoord(0,0);
			cube->position(-0.5,0.5,-0.5);cube->normal(-0.666667,0.333333,-0.666667);cube->textureCoord(0,1);
			cube->position(-0.5,-0.5,-0.5);cube->normal(-0.408248,-0.816497,-0.408248);cube->textureCoord(1,1);
			cube->position(-0.5,-0.5,0.5);cube->normal(-0.666667,-0.333333,0.666667);cube->textureCoord(1,0);
			cube->position(0.5,-0.5,-0.5);cube->normal(0.666667,-0.333333,-0.666667);cube->textureCoord(0,1);
			cube->position(0.5,0.5,-0.5);cube->normal(0.408248,0.816497,-0.408248);cube->textureCoord(1,1);
			cube->position(0.5,-0.5,0.5);cube->normal(0.408248,-0.816497,0.408248);cube->textureCoord(0,0);
			cube->position(0.5,-0.5,-0.5);cube->normal(0.666667,-0.333333,-0.666667);cube->textureCoord(1,0);
			cube->position(-0.5,-0.5,-0.5);cube->normal(-0.408248,-0.816497,-0.408248);cube->textureCoord(0,0);
			cube->position(-0.5,0.5,0.5);cube->normal(-0.408248,0.816497,0.408248);cube->textureCoord(1,0);
			cube->position(0.5,0.5,-0.5);cube->normal(0.408248,0.816497,-0.408248);cube->textureCoord(0,1);
			cube->position(-0.5,0.5,-0.5);cube->normal(-0.666667,0.333333,-0.666667);cube->textureCoord(1,1);
			cube->position(0.5,0.5,0.5);cube->normal(0.666667,0.333333,0.666667);cube->textureCoord(0,0);
			// set up the indexes
			cube->triangle(0,1,2); cube->triangle(3,1,0);
			cube->triangle(4,5,6); cube->triangle(4,7,5);
			cube->triangle(8,9,10); cube->triangle(10,7,8);
			cube->triangle(4,11,12); cube->triangle(4,13,11);
			cube->triangle(14,8,12); cube->triangle(14,15,8);
			cube->triangle(16,17,18); cube->triangle(16,19,17);
		cube->end(); 

		return cube;
	}


	bool loadTexture (const Ogre::String & texName, const Ogre::String & texPath) {
		bool loaded = false;

		std::ifstream ifs (texPath.c_str (), std::ios::binary | std::ios::in);
		if (ifs.is_open ()) {
			Ogre::String ext;
			auto extIndex = texPath.find_last_of ('.');
			if (extIndex != Ogre::String::npos) {
				ext = texPath.substr (extIndex + 1);
				Ogre::DataStreamPtr dataStream (new Ogre::FileStreamDataStream (texPath, &ifs, false));
				Ogre::Image img;
				img.load (dataStream, ext);
				Ogre::TextureManager::getSingleton ().loadImage (texName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, img, Ogre::TEX_TYPE_2D, 0, 1.0f);
				loaded = true;
			}
		}

		return loaded;
	}

	Ogre::String getCurrentDir () {
		#ifdef WIN32
			char buff [MAX_PATH];
			GetCurrentDirectory (MAX_PATH, buff);
			Ogre::String rtrn (buff);
			return rtrn;
		#else
			// TODO
		#endif
	}

	bool fileExists (const char * fname) {
		std::ifstream ifs (fname, std::ios::in);
		return ifs.good ();
	}
}