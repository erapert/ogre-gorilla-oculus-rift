#ifndef VRL_VOXEL_DEMO_H
#define VRL_VOXEL_DEMO_H 1

/*
	remember, forward declares allow us to avoid #include'ing headers here in the header
*/
namespace Ogre {
	class ManualObject;
}


/*
	our stuff
*/
namespace VRL {

	// makes a cube and returns a handle to it-- you're responsible for deleting it
	Ogre::ManualObject * createCubeMesh (Ogre::String name, Ogre::String matName);
	// loads a texture into the resource manager. Returns true on success, false on failure.
	bool loadTexture (const Ogre::String & texName, const Ogre::String & texPath);

	Ogre::String getCurrentDir ();
	bool fileExists (const char * fname);
}

#endif