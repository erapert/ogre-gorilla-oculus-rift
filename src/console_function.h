// Ŭnicode please 
#pragma once

namespace console {

	void help (Ogre::StringVector & args);
	void die (Ogre::StringVector & args);
	void about (Ogre::StringVector & args);
	void cube (Ogre::StringVector & args);
	void load (Ogre::StringVector & args);
	void cubes (Ogre::StringVector & args);

}