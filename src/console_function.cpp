#include "stdafx.h"
#include "console_function.h"
#include "OgreConsoleForGorilla.h"

#include "lib.h"
#include "vrl_voxel_demo.hpp"

namespace console {

	void help (Ogre::StringVector & args) {
		auto con = OgreConsole::getSingletonPtr ();
		con->print ("Available Commands:");
		auto commands = con->getCommands ();
		// std::map already keeps the keys sorted, it seems, but just in case it didn't this should do the trick:
		// std::sort (std::begin (commands), std::end (commands));
		for (auto c : commands) {
			con->print (c);
		}
	}

	void die (Ogre::StringVector & args) {
		// Can't call Lib::shutDown() directly because the console manager will still be attempting
		// to process callbacks even though shutDown() will have killed them all.
		// So instead we destroy the window and let the main loop shut everything down.
		Lib::window->destroy ();
	}

	void about (Ogre::StringVector & args) {
		std::stringstream s;
		s << "Ogre: " << OGRE_VERSION_MAJOR << "." << OGRE_VERSION_MINOR << "." << OGRE_VERSION_PATCH << " '" << OGRE_VERSION_NAME << "'";
		OgreConsole::getSingleton().print(s.str());
	}

	void cube (Ogre::StringVector & args) {
		auto con = OgreConsole::getSingletonPtr ();

		if (args.size() != 6) {
			con->print ("Usage: MESH_NAME MATERIAL_NAME X Y Z");
			return;
		}

		auto x = atof (args[3].c_str());
		auto y = atof (args[4].c_str());
		auto z = atof (args[5].c_str());

		auto node = Lib::scene_mgr->getRootSceneNode()->createChildSceneNode();
		node->setPosition (x, y, z);
		node->attachObject (VRL::createCubeMesh (args[1], args[2]));

		con->print ("Done.");
	}

	void load (Ogre::StringVector & args) {
		auto con = OgreConsole::getSingletonPtr();
		auto fname = args[1];

		Ogre::String constr ("loading \""); constr += fname; constr += "\"...";
		con->print (constr);

		auto cwd = VRL::getCurrentDir ();
		//con->print (cwd.c_str ());

		if (!VRL::fileExists (fname.c_str ())) {
			con->print ("File not found.");
			con->print ("1. Filenames cannot have underscores (for some reason?!)");
			constr = "2. Files must be in \""; constr += cwd; constr += "\"";
			con->print (constr);
			return;
		}

		con->print ("loading...");

		/*
		Ogre::String resourceName ("voxels");
		if (VRL::loadTexture (resourceName, fname)) {
			con->print ("Loaded OK.");
		} else {
			con->print ("Failed to load.");
		}
		*/
	}

	void cubes (Ogre::StringVector & args) {
		auto con = OgreConsole::getSingletonPtr ();

		if (args.size () != 4) {
			con->print ("usage: cubes X Y Z");
			return;
		}

		auto numCubesX = atoi (args[1].c_str ());
		auto numCubesY = atoi (args[2].c_str ());
		auto numCubesZ = atoi (args[3].c_str ());
		char numBufferX [11];
		char numBufferY [11];
		char numBufferZ [11];

		auto rootNode = Lib::scene_mgr->getRootSceneNode ();
		Ogre::String baseName ("cube_");
		Ogre::String name;
		
		/***
		// separate scene nodes are extremely slow for any dimensions over about 10 * 10 * 10
		for (auto x = 0.0f; x < numCubesX; ++x) {
			for (auto y = 0.0f; y < numCubesY; ++y) {
				for (auto z = 0.0f; z < numCubesZ; ++z) {
					auto node = rootNode->createChildSceneNode ();
					itoa (x, numBufferX, 10);
					itoa (y, numBufferY, 10);
					itoa (z, numBufferZ, 10);
					name = baseName + numBufferX;
					name += numBufferY;
					name += numBufferZ;
					node->setPosition (x + 1.0f, y + 1.0f, z + 1.0f);
					node->attachObject (VRL::createCubeMesh (name, "mat_voxels"));
				}
			}
		}
		*/

		// instanced geometry is much faster to render
		auto cube = VRL::createCubeMesh (name, "mat_voxels");
		auto cubeMPtr = cube->convertToMesh ("voxelCubeMesh");
		Ogre::MeshSerializer serializer;
		serializer.exportMesh (cubeMPtr.getPointer (), "voxel.mesh");
		auto voxelEntity = Lib::scene_mgr->createEntity ("voxels", "voxelCubeMesh");
		auto staticVoxelsGeo = Lib::scene_mgr->createStaticGeometry ("voxelsVolume");
		auto numVoxels = numCubesX * numCubesY * numCubesZ;
		
		// pre-computing these because we'll be using them a lot
		auto negHalfX = -numCubesX / 2;
		auto negHalfY = -numCubesY / 2;
		auto negHalfZ = -numCubesZ / 2;
		auto halfX = numCubesX / 2;
		auto halfY = numCubesY / 2;
		auto halfZ = numCubesZ / 2;

		// assuming cubes are 1.0 units
		staticVoxelsGeo->setRegionDimensions (Ogre::Vector3 (numCubesX, numCubesY, numCubesZ));
		staticVoxelsGeo->setOrigin (
			Ogre::Vector3 (negHalfX, negHalfY, negHalfZ)
			+
			Ogre::Vector3 (numCubesX, numCubesY, numCubesZ)
		);

		// incrementing by one because we're assuming:
		// cubesize = 1 unit per side, numcubes is a whole number, and volume size is in terms of numcubes
		Ogre::Vector3 pos;
		Ogre::Vector3 scale (1.0f, 1.0f, 1.0f);
		Ogre::Quaternion orient;
		for (auto x = negHalfX; x < halfX; x++) {
			for (auto y = negHalfY; y < halfY; y++) {
				for (auto z = negHalfZ; z < halfZ; z++) {
					pos.x = x; pos.y = y; pos.z = z;
					staticVoxelsGeo->addEntity (voxelEntity, pos, orient, scale);
				}
			}
		}
		staticVoxelsGeo->build ();

		con->print ("done.");
	}
}