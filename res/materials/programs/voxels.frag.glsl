
varying vec2 uv;
uniform sampler2D diffuseMap;

void main () {
	vec4 diffPixel = texture (diffuseMap, uv);
	diffPixel.x = 1.0;
	gl_FragColor = diffPixel;
}